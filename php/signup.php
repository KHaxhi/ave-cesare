<?php

/**
 * PHP file signup
 */

    require_once('../libraries/phpmailer/phpmailer/src/PHPMailer.php');
    require_once('../libraries/phpmailer/phpmailer/src/Exception.php');
    require_once('../libraries/phpmailer/phpmailer/src/SMTP.php');
    
    require_once('db_connect.php');
        
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $email = $_POST['email'];
        $pass1 = $_POST['pass1'];

        $mail = new PHPMailer\PHPMailer\PHPMailer(true);

        //prepare hashed password
        $vkey = md5(time() . $email);
        $password = password_hash($pass1, PASSWORD_DEFAULT);
    
        //check if user already exists using email
        $stmt = $conn->prepare("SELECT * FROM User WHERE email = ?");
        $stmt->bind_param('s', $email);
    
        $stmt->execute();
        $stmt->store_result();
    
        if ($stmt->num_rows > 0) {
            $stmt->close();
            $conn->close();
            exit ("Kjo adrese i perket nje perdoruesi tjeter.");
        }
    
        //if user doesnt exist, insert him
        $stmt = $conn->prepare("INSERT INTO User 
            SET email = ?, name = ?, surname = ?, password = ?, vkey = ?");
        $stmt->bind_param('sssss', $email, $name, $surname, $password, $vkey);
        
        $result = $stmt->execute();
    
        //send a verification email
        if ($result) {
            $mail->SMTPDebug = PHPMailer\PHPMailer\SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = $_ENV['GMAIL_SMTP_USER'];
            $mail->Password = $_ENV['GMAIL_SMTP_PASSWORD'];
            $mail->SMTPSecure = $mail::ENCRYPTION_STARTTLS;
            $mail->Port = 587;
            $mail->setFrom('no-reply@avecesare.com', 'Ave Cesare');
            $mail->AddAddress($email);
            $mail->isHTML(true);
            $mail->Subject = 'Email verification';
            $mail->Body = "<a href='https://ave-cesare.herokuapp.com/php/verify.php?vkey=$vkey'>
                Klikoni ketu per te verifikuar llogarine</a>";

            try {
                $mail->Send();
                $stmt->close();
                $conn->close();
                exit(0);
            } catch (Exception $e) {
                echo "E-mail nuk u dergua me sukses. Ju lutem provoni me vone.";
                $stmt->close();
                $conn->close();
                exit(1);
            }
        } else {
            echo $conn->error;
            $stmt->close();
            $conn->close();
            exit(1);
        }
    } else {
        header('HTTP/1.1 400 Bad Request');
        exit("Kerkesa u refuzua!");
}

?>