<?php

/**
 * PHP file to set up connection with the database
 */

$dbServername = $_ENV['DB_SERVERNAME'];
$dbUsername = $_ENV['DB_USERNAME'];
$dbPassword = $_ENV['DB_PASSWORD'];
$dbName = $_ENV['DB_NAME'];

$conn = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

if (mysqli_connect_errno()) {
  header('HTTP/1.1 500 Internal Server Error');
  exit('Lidhja me databazen deshtoi!');
}

?>